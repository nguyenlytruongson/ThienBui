﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess.BO
{
    public class AccessRightBO
    {
        public int GroupID { get; set; }
        public int AdminID { get; set; }
    }
}

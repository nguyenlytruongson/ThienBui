﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess.BO
{
    public class TransactionPointsBO
    {
        public int TransactionID { get; set; }
        public int MemberID { get; set; }
        public int NumberID { get; set; }
        public double Points { get; set; }
        public double Money { get; set; }
        public DateTime CreateDate { get; set; }
        public string Note { get; set; }
        public int TransactionStatus { get; set; }
    }
}

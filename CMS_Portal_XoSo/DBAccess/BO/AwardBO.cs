﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess.BO
{
    public class AwardBO
    {
        public int AwardID { get; set; }
        public string AwardName { get; set; }
        public string AwardValue { get; set; }
        public DateTime CreateDate { get; set; }
        public int Priority { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }

    }
}

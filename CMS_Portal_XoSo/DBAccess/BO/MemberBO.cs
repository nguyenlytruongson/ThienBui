﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess.BO
{
    public class MemberBO
    {
        public int MemberID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime CreateDate { get; set; }
        public int IsActive { get; set; }
        public int IsDelete { get; set; }
        public MemberInfomationBO MemberInfomation { get; set; }
    }
}

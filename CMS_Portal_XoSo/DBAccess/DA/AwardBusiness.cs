﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess.BO;

namespace DBAccess.DA
{
    public class AwardBusiness
    {
        public static bool InsertAward(AwardBO award, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "insert into award(AwardName,AwardValue,CreateDate,Priority,IsActive,IsDelete) values(@awardName,@awardValue,@createDate,@priority,@isActive,@isDelete)";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@awardName", award.AwardName);
                pa[1] = new SqlParameter("@isActive", award.IsActive);
                pa[2] = new SqlParameter("@isDelete", award.IsDelete);
                pa[3] = new SqlParameter("@awardValue", award.AwardValue);
                pa[4] = new SqlParameter("@createDate", award.CreateDate);
                pa[5] = new SqlParameter("@priority", award.Priority);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateAward(AwardBO award, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update award set AwardName=@awardName,AwardValue=@awardValue where AwardID=@awardID";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@awardName", award.AwardName);
                pa[1] = new SqlParameter("@awardValue", award.AwardValue);
                pa[2] = new SqlParameter("@awardID", award.AwardID);
               
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool LockAndUnlockAward(int awardID, int isActive, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update award set IsActive=@isActive where AwardID=@awardID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@isActive", isActive);
                pa[1] = new SqlParameter("@awardID", awardID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<AwardBO> GetListAward(int start, int end, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                List<AwardBO> lstAward = new List<AwardBO>();
                string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY AwardID DESC) as Row,aw.* from award aw) as Products  where Row>=@start and Row<=@end";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardBO number = new AwardBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.AwardName = reader["AwardName"].ToString();
                    number.IsActive = int.Parse(reader["IsActive"].ToString());


                    number.AwardValue = reader["AwardValue"].ToString();
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool InsertAwardNumber(AwardNumberBO award, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "insert into awardnumber(NumberID,AwardID,NumberValue,CreateDate,IsActive,IsDelete,Priority) values(@numberID,@awardID,@numberValue,@createDate,@isActive,@isDelete,@priority)";
                SqlParameter[] pa = new SqlParameter[7];
                pa[0] = new SqlParameter("@numberID", award.NumberID);
                pa[1] = new SqlParameter("@isActive", award.IsActive);
                pa[2] = new SqlParameter("@isDelete", award.IsDelete);
                pa[3] = new SqlParameter("@awardID", award.AwardID);
                pa[4] = new SqlParameter("@createDate", award.CreateDate);
                pa[5] = new SqlParameter("@priority", award.Priority);
                pa[6] = new SqlParameter("@numberValue", award.NumberValue);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public static List<AwardNumberBO> GetListAward(int start, int end, DateTime fromDate, DateTime toDate, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                List<AwardNumberBO> lstAward = new List<AwardNumberBO>();
                string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY CreateDate DESC) as Row,aw.* from awardnumber aw where CreateDate between @fromDate and @toDate ) as Products  where Row>=@start and Row<=@end";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    AwardNumberBO number = new AwardNumberBO();
                    number.AwardID = int.Parse(reader["AwardID"].ToString());
                    number.NumberValue = reader["NumberValue"].ToString();
                    number.IsActive = int.Parse(reader["IsActive"].ToString());


                    number.NumberID = int.Parse(reader["AwardValue"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstAward.Add(number);

                }
                return lstAward;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}

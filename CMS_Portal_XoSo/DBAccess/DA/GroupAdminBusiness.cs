﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess.BO;

namespace DBAccess.DA
{
    public class GroupAdminBusiness
    {
        public static bool InsertGroupAdmin(GroupAdminBO admin)
        {
            Sqlhelper helper = new Sqlhelper(ConnectionType.AdminConnection);
            try
            {
                bool rs = false;
                SqlParameter[] pa = new SqlParameter[5];
                string sql = "insert into groupadmin(GroupName,IsActive) values(@groupName,@isActive)";
                pa[0] = new SqlParameter("@groupName", admin.GroupName);
                pa[1] = new SqlParameter("@isActive", admin.IsActive);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog("Exception insert groupadmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<GroupAdminBO> ListGroupAdmin()
        {
            Sqlhelper helper = new Sqlhelper(ConnectionType.AdminConnection);
            try
            {
                List<GroupAdminBO> list = new List<GroupAdminBO>();
                string sql = "select *from groupadmin";

                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    GroupAdminBO group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());
                    list.Add(group);
                }
                return list;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static GroupAdminBO GetGroupAdminDetail(int groupID)
        {
            Sqlhelper helper = new Sqlhelper(ConnectionType.AdminConnection);
            try
            {
                GroupAdminBO group = null;
                string sql = "select *from groupadmin where GroupID=@groupID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@groupID", groupID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    group = new GroupAdminBO();
                    group.GroupID = int.Parse(reader["GroupID"].ToString());
                    group.GroupName = reader["GroupName"].ToString();
                    group.IsActive = int.Parse(reader["IsActive"].ToString());

                }
                return group;
            }
            catch
            {
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateStatusGroupAdmin(int groupID, int isActive)
        {
            Sqlhelper helper = new Sqlhelper(ConnectionType.AdminConnection);
            try
            {
                string sql = "update groupadmin set IsActive=@isActive where GroupID=@groupID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@groupID", groupID);
                pa[1] = new SqlParameter("@isActive", isActive);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog("Exception update status groupadmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateGroupAdmin(int groupID, string groupName)
        {
            Sqlhelper helper = new Sqlhelper(ConnectionType.AdminConnection);
            try
            {
                string sql = "update groupadmin set GroupName=@groupName where GroupID=@groupID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@groupID", groupID);
                pa[1] = new SqlParameter("@groupName", groupName);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                bool rs = false;
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog("Exception update groupadmin : " + ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }
    }
}

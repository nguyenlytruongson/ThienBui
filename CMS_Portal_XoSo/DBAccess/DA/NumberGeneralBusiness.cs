﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess.BO;

namespace DBAccess.DA
{
    public class NumberGeneralBusiness
    {
        public static bool InsertNumberGeneral(NumberGeneralBO number, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "insert into numbergeneral(NumberValue,Quantity,CreateDate,IsActive,IsDelete,Priority) values(@numberValue,@quantity,@createDate,@isActive,@isDelete,@priority)";
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@numberValue", number.NumberValue);
                pa[1] = new SqlParameter("@isActive", number.IsActive);
                pa[2] = new SqlParameter("@isDelete", number.IsDelete);
                pa[3] = new SqlParameter("@quantity", number.Quantity);
                pa[4] = new SqlParameter("@createDate", number.CreateDate);
                pa[5] = new SqlParameter("@priority", number.Priority);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateNumberGeneral(string numberValue, int quantity, int numberID, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update numbergeneral set NumberValue=@numberValue,Quantity=@quantity where NumberID=@numberID";
                SqlParameter[] pa = new SqlParameter[3];
                pa[0] = new SqlParameter("@numberValue", numberValue);
                pa[1] = new SqlParameter("@quantity", quantity);
                pa[2] = new SqlParameter("@numberID", numberID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateQuantityNumberGeneral(string numberValue, int quantity, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update numbergeneral set Quantity=@quantity where NumberValue=@numberValue";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@numberValue", numberValue);
                pa[1] = new SqlParameter("@quantity", quantity);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public static bool LockAndUnlockNumberGeneral(int numberID, int isActive, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update numbergeneral set IsActive=@isActive where NumberID=@numberID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@isActive", isActive);
                pa[1] = new SqlParameter("@numberID", numberID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public static NumberGeneralBO GetNumberGeneralDetail(int numberID, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                NumberGeneralBO number = null;
                string sql = "select * from numbergeneral where NumberID=@numberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@numberID", numberID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    number = new NumberGeneralBO();
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.NumberValue = reader["NumberValue"].ToString();
                    number.Quantity = int.Parse(reader["Quantity"].ToString());


                    number.Priority = int.Parse(reader["Priority"].ToString());

                }
                return number;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool CheckNumberExists(string numberValue, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "select NumberID from numbergeneral where NumberValue=@numberValue";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@numberValue", numberValue);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<NumberGeneralBO> GetListNumberGeneral(int start, int end, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                List<NumberGeneralBO> lstNumber = new List<NumberGeneralBO>();
                string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY NumberID DESC) as Row,ng.* from numbergeneral ng) as Products  where Row>=@start and Row<=@end";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    NumberGeneralBO number = new NumberGeneralBO();
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.NumberValue = reader["NumberValue"].ToString();
                    number.IsActive = int.Parse(reader["IsActive"].ToString());


                    number.Quantity = int.Parse(reader["Quantity"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstNumber.Add(number);

                }
                return lstNumber;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<NumberGeneralBO> GetListNumberGeneral(int start, int end,DateTime fromDate, DateTime toDate, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                List<NumberGeneralBO> lstNumber = new List<NumberGeneralBO>();
                string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY NumberID DESC) as Row,ng.* from numbergeneral ng where ng.CreateDate between @fromDate and @toDate) as Products  where Row>=@start and Row<=@end";
                SqlParameter[] pa = new SqlParameter[4];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                pa[2] = new SqlParameter("@fromDate", fromDate);
                pa[3] = new SqlParameter("@toDate", toDate);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    NumberGeneralBO number = new NumberGeneralBO();
                    number.NumberID = int.Parse(reader["NumberID"].ToString());
                    number.NumberValue = reader["NumberValue"].ToString();
                    number.IsActive = int.Parse(reader["IsActive"].ToString());


                    number.Quantity = int.Parse(reader["Quantity"].ToString());
                    number.Priority = int.Parse(reader["Priority"].ToString());
                    number.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
                    lstNumber.Add(number);

                }
                return lstNumber;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }


    }
}

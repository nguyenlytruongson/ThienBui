﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DBAccess.BO;

namespace DBAccess.DA
{
    public class MemberBusiness
    {
        public static int InsertMember(MemberBO member, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                int memberID = 0;
                string sql = "insert into member(UserName,Password,IsActive,IsDelete,CreateDate) values(@username,@password,@isActive,@isDelete,@createDate); SELECT SCOPE_IDENTITY()";
                SqlParameter[] pa = new SqlParameter[5];
                pa[0] = new SqlParameter("@username", member.UserName);
                pa[1] = new SqlParameter("@isActive", member.IsActive);
                pa[2] = new SqlParameter("@isDelete", member.IsDelete);
                pa[3] = new SqlParameter("@password", member.Password);
                pa[4] = new SqlParameter("@createDate", member.CreateDate);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                memberID = Convert.ToInt32(command.ExecuteScalar());
                return memberID;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool InsertMemberInfomation(MemberInfomationBO member, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "insert into memberinfomation(MemberID,FullName,Email,Mobile,BirthDay,Address,Gender,Points,Avatar) values(@memberID,@fullname,@email,@mobile,@birthday,@addressMember,@gender,@points,@avatar)";
                SqlParameter[] pa = new SqlParameter[9];
                pa[0] = new SqlParameter("@memberID", member.MemberID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);
               
                pa[3] = new SqlParameter("@mobile", member.Mobile);
         
                pa[4] = new SqlParameter("@gender", member.Gender);
                if (member.Birthday != null)
                {
                    pa[5] = new SqlParameter("@birthday", member.Birthday);
                }
                else
                {
                    pa[5] = new SqlParameter("@birthday", DBNull.Value);
                }
             
                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);
             
                pa[8] = new SqlParameter("@points", member.Points);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdateMemberInfomation(MemberInfomationBO member, int memberID, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update memberinfomation set FullName=@fullName,Email=@email,Mobile=@mobile,Gender=@gender,BirthDay=@birthday,Avatar=@avatar,Address=@addressMember where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[8];
                pa[0] = new SqlParameter("@memberID", memberID);
                pa[1] = new SqlParameter("@fullname", member.FullName);
                pa[2] = new SqlParameter("@email", member.Email);
               
                pa[3] = new SqlParameter("@mobile", member.Mobile);
             
                pa[4] = new SqlParameter("@gender", member.Gender);
                pa[5] = new SqlParameter("@birthday", member.Birthday);
          
                pa[6] = new SqlParameter("@avatar", member.Avatar);
                pa[7] = new SqlParameter("@addressMember", member.Address);
             
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {

                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool UpdatePasswordMember(string userName, string password, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update member set PasswordUser=@password where UserName=@userName";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@username", userName);
                pa[1] = new SqlParameter("@password", password);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool LockAndUnlockMember(int memberID, int isActive, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "update member set IsActive=@isActive where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@isActive", isActive);
                pa[1] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                int row = command.ExecuteNonQuery();
                if (row > 0)
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }


        public static MemberInfomationBO GetMemberDetail(int memberID, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                MemberInfomationBO member = null;
                string sql = "select * from memberinfomation where MemberID=@memberID";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();

                    member.Email = reader["Email"].ToString();


                    member.Mobile = reader["Mobile"].ToString();

                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();

                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());

                }
                return member;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static MemberInfomationBO GetMemberDetail(string email, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                MemberInfomationBO member = null;
                string sql = "select p.*, m.UserName from memberinfomation p left join member m on m.MemberID=p.MemberID where Email=@email";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();
                   
                    member.Email = reader["Email"].ToString();
                   
               
                    member.Mobile = reader["Mobile"].ToString();
               
                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();
                   
                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());

                }
                return member;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        //public static MemberProfileBO LoginMember(string userName, string password, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        MemberProfileBO member = null;
        //        string sql = "select p.*,m.UserName,m.PasswordUser from member m, memberprofile p where UserName=@userName and PasswordUser=@password and p.MemberID=m.MemberID";
        //        SqlParameter[] pa = new SqlParameter[2];
        //        pa[0] = new SqlParameter("@userName", userName);
        //        pa[1] = new SqlParameter("@password", password);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            member = new MemberProfileBO();
        //            member.MemberID = int.Parse(reader["MemberID"].ToString());
        //            member.FullName = reader["FullName"].ToString();
        //            member.Password = reader["PasswordUser"].ToString();
        //            member.UserName = reader["UserName"].ToString();
        //            member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
        //            member.Email = reader["Email"].ToString();
        //            member.DistrictID = int.Parse(reader["DistrictID"].ToString());
        //            member.Phone = reader["Phone"].ToString();
        //            member.Mobile = reader["Mobile"].ToString();
        //            member.WardID = int.Parse(reader["WardID"].ToString());
        //            member.TypeAccount = int.Parse(reader["TypeAccount"].ToString());
        //            member.Gender = int.Parse(reader["Gender"].ToString());
        //            member.Avatar = reader["Avatar"].ToString();
        //            if (!string.IsNullOrEmpty(reader["AddressMember"].ToString()))
        //            {
        //                member.Adress = reader["AddressMember"].ToString();
        //            }
        //            else
        //            {

        //            }
        //            member.Infomation = reader["Infomation"].ToString();
        //            member.SkypeID = reader["SkypeID"].ToString();
        //            member.YahooID = reader["YahooID"].ToString();
        //            if (!string.IsNullOrEmpty(reader["Birthday"].ToString()))
        //            {
        //                member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
        //            }
        //            member.IsSendMessage = int.Parse(reader["IsSendMessage"].ToString());
        //           // member.AgentObj = AgencyBusiness.GetAgencyDetail(member.MemberID, ConnectionType.UserConnection);
        //        }
        //        return member;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}


        public static bool CheckUserNameExists(string userName, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "select UserName from member where UserName=@userName";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@userName", userName);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static bool CheckEmailExists(string email, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                bool rs = false;
                string sql = "select Email from memberinfomation where Email=@email";
                SqlParameter[] pa = new SqlParameter[1];
                pa[0] = new SqlParameter("@email", email);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    rs = true;
                }
                return rs;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return false;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<MemberInfomationBO> GetListMember(ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                List<MemberInfomationBO> lstMember = new List<MemberInfomationBO>();
                string sql = "select m.UserName,p.* from memberinfomation p left join member m on m.MemberID=p.MemberID order by m.MemberID DESC";
                //SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@memberID", memberID);
                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberInfomationBO member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();
                    //member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
                    member.Email = reader["Email"].ToString();
                    //member.DistrictID = int.Parse(reader["DistrictID"].ToString());
                    //member.Phone = reader["Phone"].ToString();
                    //member.WardID = int.Parse(reader["WardID"].ToString());
                    //member.TypeAccount = int.Parse(reader["TypeAccount"].ToString());
                    member.Gender = int.Parse(reader["Gender"].ToString());
                    member.Avatar = reader["Avatar"].ToString();
                    member.Address = reader["Address"].ToString();
                    member.Mobile = reader["Mobile"].ToString();
                    member.Birthday = DateTime.Parse(reader["Birthday"].ToString());
                    member.Points = double.Parse(reader["Points"].ToString());
                    lstMember.Add(member);

                }
                return lstMember;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        public static List<MemberInfomationBO> GetListMemberInfomation(int start, int end, ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                List<MemberInfomationBO> lstMember = new List<MemberInfomationBO>();
                string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.MemberID DESC) as Row,m.MemberID,mb.UserName,m.FullName,m.Avatar,m.Mobile,m.Email,m.Points from memberinfomation m left join member mb on m.MemberID=n.MemberID where group by m.MemberID,mb.UserName,m.FullName,m.Avatar,m.Mobile,m.Email,m.Points) as Products  where Row>=@start and Row<=@end";
                SqlParameter[] pa = new SqlParameter[2];
                pa[0] = new SqlParameter("@start", start);
                pa[1] = new SqlParameter("@end", end);
                SqlCommand command = helper.GetCommand(sql, pa, false);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    MemberInfomationBO member = new MemberInfomationBO();
                    member.MemberID = int.Parse(reader["MemberID"].ToString());
                    member.UserName = reader["UserName"].ToString();
                    member.FullName = reader["FullName"].ToString();

                    member.Email = reader["Email"].ToString();

                    member.Avatar = reader["Avatar"].ToString();
                    member.Mobile = reader["Mobile"].ToString();
                    member.Points = double.Parse(reader["Points"].ToString());
                    lstMember.Add(member);

                }
                return lstMember;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return null;
            }
            finally
            {
                helper.destroy();
            }
        }

        //public static List<MemberHobbyBO> GetListMemberHobbyAdmin(int start, int end, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        List<MemberHobbyBO> lstMember = new List<MemberHobbyBO>();
        //        string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,m.*,mp.FullName,mp.Email from memberhobby m,memberprofile mp where m.MemberID=mp.MemberID and mp.IsActive=1 and mp.IsDelete=0) as Products  where Row>=@start and Row<=@end";
        //        SqlParameter[] pa = new SqlParameter[2];
        //        pa[0] = new SqlParameter("@start", start);
        //        pa[1] = new SqlParameter("@end", end);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            MemberHobbyBO member = new MemberHobbyBO();
        //            member.MemberID = int.Parse(reader["MemberID"].ToString());
        //            member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
        //            member.DistrictID = int.Parse(reader["DistrictID"].ToString());
        //            member.SquareID = int.Parse(reader["SquareID"].ToString());
        //            member.PriceID = int.Parse(reader["PriceID"].ToString());
        //            member.DirectionID = int.Parse(reader["DirectionID"].ToString());
        //            member.CategoryID = int.Parse(reader["CategoryID"].ToString());
        //            member.FullName = reader["FullName"].ToString();
        //            member.Email = reader["Email"].ToString();

        //            lstMember.Add(member);

        //        }
        //        return lstMember;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        public static int CountAllMember(ConnectionType typeConnect)
        {
            Sqlhelper helper = new Sqlhelper(typeConnect);
            try
            {
                int count = 0;
                string sql = "select count(MemberID) as totalMember from memberprofile";
                //SqlParameter[] pa = new SqlParameter[1];
                //pa[0] = new SqlParameter("@isActive", isActive);
                SqlCommand command = helper.GetCommandNonParameter(sql, false);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    count = int.Parse(reader["totalMember"].ToString());

                }
                return count;
            }
            catch (Exception ex)
            {
                Utilitys.WriteLog(ex.Message);
                return 0;
            }
            finally
            {
                helper.destroy();
            }
        }

        //public static int CountAllMemberHobby(ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        int count = 0;
        //        string sql = "select count(MemberID) as totalMember from memberhobby";
        //        //SqlParameter[] pa = new SqlParameter[1];
        //        //pa[0] = new SqlParameter("@isActive", isActive);
        //        SqlCommand command = helper.GetCommandNonParameter(sql, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            count = int.Parse(reader["totalMember"].ToString());

        //        }
        //        return count;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return 0;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static bool InsertMemberHobby(MemberHobbyBO member, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        bool rs = false;
        //        string sql = "insert into memberhobby(MemberID,ProvinceID,DistrictID,SquareID,PriceID,DirectionID,CategoryID,CreateDate,UpdateDate) values(@memberID,@provinceID,@districtID,@squareID,@priceID,@directionID,@categoryID,@createDate,@updateDate)";
        //        SqlParameter[] pa = new SqlParameter[9];
        //        pa[0] = new SqlParameter("@memberID", member.MemberID);
        //        pa[1] = new SqlParameter("@provinceID", member.ProvinceID);
        //        pa[2] = new SqlParameter("@districtID", member.DistrictID);
        //        pa[3] = new SqlParameter("@squareID", member.SquareID);
        //        pa[4] = new SqlParameter("@priceID", member.PriceID);
        //        pa[5] = new SqlParameter("@categoryID", member.CategoryID);

        //        pa[6] = new SqlParameter("@createDate", member.CreateDate);
        //        pa[7] = new SqlParameter("@updateDate", member.UpdateDate);
        //        pa[8] = new SqlParameter("@directionID", member.DirectionID);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        int row = command.ExecuteNonQuery();
        //        if (row > 0)
        //        {
        //            rs = true;
        //        }
        //        return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static bool UpdateMemberHobby(MemberHobbyBO member, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        bool rs = false;
        //        string sql = "update memberhobby set ProvinceID=@provinceID,DistrictID=@districtID,SquareID=@squareID,PriceID=@priceID,DirectionID=@directionID,CategoryID=@categoryID,UpdateDate=@updateDate where MemberID=@memberID";
        //        SqlParameter[] pa = new SqlParameter[8];
        //        pa[0] = new SqlParameter("@memberID", member.MemberID);
        //        pa[1] = new SqlParameter("@provinceID", member.ProvinceID);
        //        pa[2] = new SqlParameter("@districtID", member.DistrictID);
        //        pa[3] = new SqlParameter("@squareID", member.SquareID);
        //        pa[4] = new SqlParameter("@priceID", member.PriceID);
        //        pa[5] = new SqlParameter("@categoryID", member.CategoryID);
        //        pa[6] = new SqlParameter("@updateDate", member.UpdateDate);
        //        pa[7] = new SqlParameter("@directionID", member.DirectionID);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        int row = command.ExecuteNonQuery();
        //        if (row > 0)
        //        {
        //            rs = true;
        //        }
        //        return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static bool DeleteMemberHobby(int memberID, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        bool rs = false;
        //        string sql = "delete memberhobby where MemberID=@memberID";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@memberID", memberID);

        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        int row = command.ExecuteNonQuery();
        //        if (row > 0)
        //        {
        //            rs = true;
        //        }
        //        return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static List<MemberHobbyBO> GetListMemberHobby(int start, int end, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        List<MemberHobbyBO> lstMember = new List<MemberHobbyBO>();
        //        string sql = "select *from (SELECT ROW_NUMBER() OVER (ORDER BY m.CreateDate DESC) as Row,m.*,mb.UserName,p.ProvinceName,d.DistrictName,s.SquareName,pr.PriceName,c.Categoryname,dr.DirectionName from memberhobby m,province p, member mb, district d,priceproperty pr, squareproperty s,direction dr,categorynews c  where m.MemberID=mb.MemberID and m.ProvinceID=p.ProvinceID and m.DistrictID=d.DistrictID and m.SquareID=s.SquareID and m.PriceID=pr.PriceID and m.DirectionID=dr.DirectionID and m.CategoryID=c.CategoryID ) as Products  where Row>=@start and Row<=@end";
        //        SqlParameter[] pa = new SqlParameter[2];
        //        pa[0] = new SqlParameter("@start", start);
        //        pa[1] = new SqlParameter("@end", end);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            MemberHobbyBO member = new MemberHobbyBO();
        //            member.MemberID = int.Parse(reader["MemberID"].ToString());
        //            member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
        //            member.DistrictID = int.Parse(reader["DistrictID"].ToString());
        //            member.SquareID = int.Parse(reader["SquareID"].ToString());
        //            member.PriceID = int.Parse(reader["PriceID"].ToString());
        //            member.DirectionID = int.Parse(reader["DirectionID"].ToString());
        //            member.CategoryID = int.Parse(reader["CategoryID"].ToString());
        //            member.UserName = reader["UserName"].ToString();
        //            member.ProvinceName = reader["ProvinceName"].ToString();
        //            member.DistrictName = reader["DistrictName"].ToString();
        //            member.SquareName = reader["SquareName"].ToString();
        //            member.PriceName = reader["PriceName"].ToString();
        //            member.CategoryName = reader["CategoryName"].ToString();
        //            member.DirectionName = reader["DirectionName"].ToString();
        //            member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
        //            member.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
        //            lstMember.Add(member);

        //        }
        //        return lstMember;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static List<MemberHobbyBO> GetListHobbyByMember(int memberID, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        List<MemberHobbyBO> lstMember = new List<MemberHobbyBO>();
        //        string sql = "select *from memberhobby where MemberID=@memberID";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@memberID", memberID);

        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        while (reader.Read())
        //        {
        //            MemberHobbyBO member = new MemberHobbyBO();
        //            member.MemberID = int.Parse(reader["MemberID"].ToString());
        //            member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
        //            member.DistrictID = int.Parse(reader["DistrictID"].ToString());
        //            member.SquareID = int.Parse(reader["SquareID"].ToString());
        //            member.PriceID = int.Parse(reader["PriceID"].ToString());
        //            member.DirectionID = int.Parse(reader["DirectionID"].ToString());
        //            member.CategoryID = int.Parse(reader["CategoryID"].ToString());

        //            member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
        //            member.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());
        //            lstMember.Add(member);

        //        }
        //        return lstMember;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static MemberHobbyBO GetMemberHobbyDetail(int memberID, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        MemberHobbyBO member = null;
        //        string sql = "select m.* from memberhobby m where m.MemberID=@memberID";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@memberID", memberID);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            member = new MemberHobbyBO();
        //            member.MemberID = int.Parse(reader["MemberID"].ToString());
        //            member.ProvinceID = int.Parse(reader["ProvinceID"].ToString());
        //            member.DistrictID = int.Parse(reader["DistrictID"].ToString());
        //            member.SquareID = int.Parse(reader["SquareID"].ToString());
        //            member.PriceID = int.Parse(reader["PriceID"].ToString());
        //            member.DirectionID = int.Parse(reader["DirectionID"].ToString());
        //            member.CategoryID = int.Parse(reader["CategoryID"].ToString());

        //            member.CreateDate = DateTime.Parse(reader["CreateDate"].ToString());
        //            member.UpdateDate = DateTime.Parse(reader["UpdateDate"].ToString());


        //        }
        //        return member;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return null;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}

        //public static bool CheckMemberExistHobby(int memberID, ConnectionType typeConnect)
        //{
        //    Sqlhelper helper = new Sqlhelper(typeConnect);
        //    try
        //    {
        //        bool rs = false;
        //        string sql = "select MemberID from memberhobby where MemberID=@memberID";
        //        SqlParameter[] pa = new SqlParameter[1];
        //        pa[0] = new SqlParameter("@memberID", memberID);
        //        SqlCommand command = helper.GetCommand(sql, pa, false);
        //        SqlDataReader reader = command.ExecuteReader();
        //        if (reader.Read())
        //        {
        //            rs = true;
        //        }
        //        return rs;
        //    }
        //    catch (Exception ex)
        //    {
        //        Utilitys.WriteLog(ex.Message);
        //        return false;
        //    }
        //    finally
        //    {
        //        helper.destroy();
        //    }
        //}
    }
}

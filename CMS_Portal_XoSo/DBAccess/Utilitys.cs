﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DBAccess
{
    public class Utilitys
    {
        public static List<string> TransferStringToList(string value)
        {
            try
            {
                List<string> list = new List<string>();
                if (value != "")
                {
                    string[] arr = value.Split(',');
                    for (int i = 0; i < arr.Length; i++)
                    {
                        list.Add(arr[i]);
                    }
                }
                return list;
            }
            catch
            {
                return null;
            }
        }

        public static string TransferListToString(List<string> list)
        {
            try
            {
                string value = "";
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        value += list[i];
                        if (i < list.Count - 1)
                        {
                            value += ',';
                        }
                    }
                }
                return value;
            }
            catch
            {
                return null;
            }
        }

        public static void WriteLog(string logEvent)
        {

            try
            {
                FileStream fileStream;
                //string rootPath = AppDomain.CurrentDomain.BaseDirectory;
                string logfileName = AppDomain.CurrentDomain.BaseDirectory + "Log/" + DateTime.Now.ToShortDateString().Replace('/', '_') + ".txt";
                if (File.Exists(logfileName) == false)
                    fileStream = File.Create(logfileName);
                else
                {
                    FileInfo logFileInfo = new FileInfo(logfileName);
                    if (logFileInfo.Length > 500000)
                    {
                        File.Delete(logfileName);
                        fileStream = File.Create(logfileName);
                    }
                    else
                    {
                        fileStream = File.Open(logfileName, FileMode.Append);
                    }
                }

                logEvent = DateTime.Now.ToString() + " " + logEvent + "\r\n \r\n";

                byte[] info = new UTF8Encoding(true).GetBytes(logEvent);
                fileStream.Write(info, 0, logEvent.Length);
                fileStream.Close();
            }


            catch
            {

            }
            finally
            {

            }

        }

        public static string ConvetToUnSign(string value)
        {
            try
            {
                Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
                string temp = value.Normalize(NormalizationForm.FormD);
                return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
            }
            catch
            {
                return value;
            }
        }

        public static bool CheckDay(string value)
        {
            try
            {
                string month = value.Substring(0, 2);
                string day = value.Substring(3, 2);
                string year = value.Substring(6, 4);
                int numberMonth = int.Parse(month);
                int numberDay = int.Parse(day);
                int numberYear = int.Parse(year);
                bool result = true;
                switch (numberMonth)
                {
                    case 2:
                        {
                            if (numberYear % 4 == 0 || numberYear % 100 == 0)
                            {
                                if (numberDay > 27)
                                {
                                    result = false;
                                    //break;
                                }
                            }
                            else
                                if (numberDay > 28)
                                {
                                    result = false;


                                }
                            break;
                        }
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        {
                            if (numberDay > 31)
                            {
                                result = false;
                            }
                            break;
                        }
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        {
                            if (numberDay > 30)
                            {
                                result = false;
                            }
                            break;
                        }

                }
                return result;

            }
            catch
            {
                return false;
            }
        }

        public static int GetMaxDayFollowMonth(int month, int year)
        {
            try
            {
                int valueDay = 30;
                switch (month)
                {
                    case 2:
                        {
                            if (year % 4 == 0 || year % 100 == 0)
                            {
                                valueDay = 29;
                            }
                            else
                            {
                                valueDay = 28;
                            }
                            break;
                        }
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 8:
                    case 10:
                    case 12:
                        {
                            valueDay = 31;
                            break;
                        }
                    case 4:
                    case 6:
                    case 9:
                    case 11:
                        {
                            valueDay = 30;
                            break;
                        }

                }
                return valueDay;

            }
            catch
            {
                return 0;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBAccess
{
    public class WebConfig
    {
        public static string USER_CONNECT_STRING = "USER_CONNECT_STRING";
        public static string ADMIN_CONNECT_STRING = "ADMIN_CONNECT_STRING";

        public static void loadWebConfig()
        {

            if (USER_CONNECT_STRING != null)
            {
                try
                {
                    USER_CONNECT_STRING = ConfigurationManager.ConnectionStrings[USER_CONNECT_STRING].ConnectionString;
                }
                catch (Exception e)
                {
                    Utilitys.WriteLog("USER_CONNECT_STRING : " + e.Message);
                }
            }

            if (ADMIN_CONNECT_STRING != null)
            {
                try
                {
                    ADMIN_CONNECT_STRING = ConfigurationManager.ConnectionStrings[ADMIN_CONNECT_STRING].ConnectionString;
                }
                catch (Exception e)
                {
                    Utilitys.WriteLog("ADMIN_CONNECT_STRING : " + e.Message);
                }

            }

            //Utilitys.WriteLog("ADMIN_CONNECT_STRING : " + ADMIN_CONNECT_STRING);
            //Utilitys.WriteLog("ADMIN_CONNECT_STRING : " + USER_CONNECT_STRING);
        }
    }
}
